provider "aws" {
    region = var.aws_region
}

# create security group with firewall rules
resource "aws_security_group" "my_security_group" {
    name = var.security_group
    description = "Security group for EC2 instance"

    # allow access on port 8080
    ingress {
        from_port        = 8080
        to_port          = 8080
        protocol         = "tcp"
        cidr_blocks      = ["0.0.0.0/0"]
    }

    # outbound from Jenkins server
    ingress {
        from_port        = 0
        to_port          = 65535
        protocol         = "tcp"
        cidr_blocks      = ["0.0.0.0/0"]
    }

    tags= {
        Name = var.security_group
    }
}

# create AWS key pair for second instance
resource "aws_key_pair" "app_key" {
  key_name   = "app_key"
  public_key = tls_private_key.rsa.public_key_openssh
}

# RSA key of size 4096 bits
resource "tls_private_key" "rsa" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

# print to cli private key for second instance to computer
output "private_key" {
  value     = tls_private_key.rsa.private_key_pem
  sensitive = true
}

# create first AWS EC2 instance
resource "aws_instance" "myFirstInstance" {
    ami = var.ami_id
    key_name = var.key_name
    instance_type = var.instance_type
    security_groups = [var.security_group]
    tags= {
        Name = var.tag_name
    }
}

# create second AWS EC2 instance
resource "aws_instance" "mySecondInstance" {
    ami = var.ami_id
    key_name = var.key_name
    user_data = <<-EOF
    #!/bin/bash
    set -ex
    sudo yum update -y
    sudo amazon-linux-extras install docker -y
    sudo service docker start
    sudo usermod -a -G docker ec2-user
    sudo curl -L https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
  EOF
    instance_type = var.instance_type
    security_groups = [var.security_group]
    tags= {
        Name = var.tag_name_2
    }
}

# create elastic IP address
resource "aws_eip" "myFirstInstance" {
    vpc = true
    instance = aws_instance.myFirstInstance.id
    tags= {
        Name = "my_elastic_ip"
    }
}
